package main

import "gitlab.com/ethan.reesor/vscode-notebooks/yaegi/_test/foo"

func main() { println(foo.Bar, foo.Boo) }

// Output:
// init boo
// init foo
// BARR Boo
