package main

import (
	"gitlab.com/ethan.reesor/vscode-notebooks/yaegi/interp"
)

func main() {
	i := interp.New(interp.Opt{})
	i.Eval(`println("Hello")`)
}

// Output:
// Hello
