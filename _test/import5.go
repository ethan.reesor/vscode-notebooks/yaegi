package main

import boo "gitlab.com/ethan.reesor/vscode-notebooks/yaegi/_test/foo"

func main() { println(boo.Bar, boo.Boo, boo.Bir) }

// Output:
// init boo
// init foo
// BARR Boo Boo22
