package main

import (
	"fmt"

	"gitlab.com/ethan.reesor/vscode-notebooks/yaegi/_test/method38"
)

func main() {
	fmt.Println(method38.Get())
}

// Output:
// &{[] {<nil>}}
