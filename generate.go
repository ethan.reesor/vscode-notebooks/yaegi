package yaegi

//go:generate go generate gitlab.com/ethan.reesor/vscode-notebooks/yaegi/internal/cmd/extract
//go:generate go generate gitlab.com/ethan.reesor/vscode-notebooks/yaegi/interp
//go:generate go generate gitlab.com/ethan.reesor/vscode-notebooks/yaegi/stdlib
//go:generate go generate gitlab.com/ethan.reesor/vscode-notebooks/yaegi/stdlib/syscall
//go:generate go generate gitlab.com/ethan.reesor/vscode-notebooks/yaegi/stdlib/unsafe
